lower_bound = 353096
upper_bound = 843212


# STAGE 1 RULES:
# - Six digit number
# - Within range of given input
# - Has a double digit
# - Digits never decrease (left -> right)


def check_number(number: str):
    double_digit = False
    decreasing = False
    if int(number) < lower_bound or int(number) > upper_bound:
        return False

    chars = set(number)
    for c in chars:
        matches = 0
        for n in number:
            if c == n:
                matches += 1
        if matches == 2:
            double_digit = True
            break
    for i in range(0, len(number) - 1):
        if int(number[i]) > int(number[i + 1]):
            decreasing = True

    return double_digit and not decreasing


total_count = 0
num_count = 0
for a in range(0, 10):
    for b in range(0, 10):
        for c in range(0, 10):
            for d in range(0, 10):
                for e in range(0, 10):
                    for f in range(0, 10):
                        total_count += 1
                        number = str(a) + str(b) + str(c) + str(d) + str(e) + str(f)
                        if check_number(number):
                            num_count += 1

print(f"Total Count: {total_count}")
print(f"Hits: {num_count}")

# part one: 579
# part two: 358
