import itertools

ADD = 1
MULT = 2
END = 99

SPECIAL_VALUE = 19690720

special_pair = [0, 0]
for noun, verb in itertools.product(range(0, 100), range(0, 100)):
    
    intcodes = None
    with open('data.in') as f:
        data = f.read()
        intcodes = [int(i) for i in data.split(',')]
    
    intcodes[1] = noun
    intcodes[2] = verb

    opcode = intcodes[0]
    i = 0
    while i < len(intcodes)-3 and opcode != END:
        if opcode == ADD:
            intcodes[intcodes[i+3]] = intcodes[intcodes[i+1]] + intcodes[intcodes[i+2]]
        elif opcode == MULT:
            intcodes[intcodes[i+3]] = intcodes[intcodes[i+1]] * intcodes[intcodes[i+2]]
        else:
            raise ValueError(f"Undefined opcode at {i}:{opcode}!")
        i += 4
        opcode = intcodes[i]

    if intcodes[0] == SPECIAL_VALUE:
        special_pair[0] = noun
        special_pair[1] = verb

special_value = special_pair[0] * 100 + special_pair[1]
print(special_value)