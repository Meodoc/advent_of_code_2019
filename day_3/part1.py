import numpy as np
import os
import matplotlib.pyplot as plt


def gen_grid(route):
    pointer = [0, 0]
    grid = []
    for direction in route:
        if direction[0] == 'U':
            for z in range(0, int(direction.replace('U', ''))):
                pointer = (pointer[0], pointer[1] + 1)
                grid.append(pointer)
        elif direction[0] == 'D':
            for z in range(0, int(direction.replace('D', ''))):
                pointer = (pointer[0], pointer[1] - 1)
                grid.append(pointer)
        elif direction[0] == 'L':
            for z in range(0, int(direction.replace('L', ''))):
                pointer = (pointer[0] + 1, pointer[1])
                grid.append(pointer)
        elif direction[0] == 'R':
            for z in range(0, int(direction.replace('R', ''))):
                pointer = (pointer[0] - 1, pointer[1])
                grid.append(pointer)
    return grid


infile = open(r"C:\Users\Lukas\Documents\GitKraken Projects\advent_of_code_2019\day_3\data.in", 'r')
cableA = infile.readline().split(",")
cableB = infile.readline().split(",")
aGrid = gen_grid(cableA)
bGrid = gen_grid(cableB)

minSteps = 99999999999999999999999999
total = len(aGrid)
curr = 0
stepsA = 0
stepsB = 0
for coordA in aGrid:
    stepsA += 1
    stepsB = 0
    for coordB in bGrid:
        stepsB += 1
        if stepsA + stepsB > minSteps:
            break
        if coordA == coordB != (0, 0):
            steps = stepsA + stepsB
            print("intersection at " + str(coordA) + " with " + str(steps) + " steps")
            if steps < minSteps:
                minSteps = steps
                print(minSteps)
print(minSteps)
