import sys
import matplotlib.pyplot as plt


def parse_wire(wire, flag):
    coords = []
    c_x = 0
    c_y = 0
    steps = 0

    LEFT = 'L'
    RIGHT = 'R'
    UP = 'U'
    DOWN = 'D'

    print("-- Start Wire Parsing --")
    for step in wire.split(','):
        dir = step[0]
        range_ = int(step[1:]) + 1
        if dir == RIGHT:
            for j in range(1, range_):
                coords.append((c_x, c_y, flag, steps))
                steps += 1
                c_x += 1
        elif dir == LEFT:
            for j in range(1, range_):
                coords.append((c_x, c_y, flag, steps))
                steps += 1
                c_x -= 1
        elif dir == UP:
            for j in range(1, range_):
                coords.append((c_x, c_y, flag, steps))
                steps += 1
                c_y += 1
        elif dir == DOWN:
            for j in range(1, range_):
                coords.append((c_x, c_y, flag, steps))
                steps += 1
                c_y -= 1
        else:
            raise ValueError("Invalid direction found")
    return coords


def get_min_step_intersection(coords):
    min_ = sys.maxsize
    # start by 3 to not use the 0 coords and first coord
    for i in range(0, len(coords) - 1):
        if (coords[i][0] == coords[i + 1][0] and  # x coords match
                coords[i][1] == coords[i + 1][1] and  # y coords match
                coords[i][2] != coords[i + 1][2]):  # wire flags differ
            wire_length = coords[i][3] + coords[i + 1][3]
            print(f"Intersection found: x:{coords[i][0]}, y:{coords[i][1]}")
            if wire_length < min_:
                min_ = wire_length
                print(f"New min length found: {min_}")
    return min_


wire1 = []
wire2 = []
with open('data.in') as f:
    data = f.read()
    wire1, wire2 = data.splitlines()

coords1 = parse_wire(wire1, 1)
coords2 = parse_wire(wire2, 2)

print(f"Length Coords1: {len(coords1)}")
print(f"Length Coords2: {len(coords2)}")
print("-- Wire Parsing Complete --\n")

print("-- Start Array Sorting")
coords = coords1[1:] + coords2[1:]
coords = sorted(coords, key=lambda element: (element[0], element[1]))
print(f"Length Coords: {len(coords)}")
print("-- Array Sorting Complete --\n")

print("-- Start Intersection Search --")
min_stepsize_intersection = get_min_step_intersection(coords)
print("-- Intersection Search Complete --\n")

print(f"Min intersction length: {min_stepsize_intersection}\n")

plot_coords_1 = []
plot_coords_2 = []
[plot_coords_1.append((coord[0], coord[1])) for coord in coords1]
[plot_coords_2.append((coord[0], coord[1])) for coord in coords2]
print(f"-- Start Plotting Results --")
plt.scatter(*zip(*plot_coords_1), marker=',', s=1)
plt.scatter(*zip(*plot_coords_2), marker=',', s=1)
print(f"-- Plotting Results Complete --")
plt.show()

# solution_1: 1431
# solution_2: 48012
