import math
import sys

wire1 = []
wire2 = []

with open('data.in') as f:
    data = f.read()
    wire1, wire2 = data.splitlines()

LEFT = 'L'
RIGHT = 'R'
UP = 'U'
DOWN = 'D'

# Parse wires
coords1 = []
c_x = 0
c_y = 0
steps = 0

for step in wire1.split(','):
    dir = step[0]
    range_ = int(step[1:]) + 1
    if dir == RIGHT:
        for j in range(1, range_):
            coords1.append((c_x, c_y, steps))
            steps += 1
            c_x += 1
    elif dir == LEFT:
        for j in range(1, range_):
            coords1.append((c_x, c_y, steps))
            steps += 1
            c_x -= 1
    elif dir == UP:
        for j in range(1, range_):
            coords1.append((c_x, c_y, steps))
            steps += 1
            c_y += 1
    elif dir == DOWN:
        for j in range(1, range_):
            coords1.append((c_x, c_y, steps))
            steps += 1
            c_y -= 1
    else:
        raise ValueError("Invalid direction found")

coords2 = []
c_x = 0
c_y = 0
steps = 0

for step in wire2.split(','):
    dir = step[0]
    range_ = int(step[1:]) + 1
    if dir == RIGHT:
        for j in range(1, range_):
            coords2.append((c_x, c_y, steps))
            steps += 1
            c_x += 1
    elif dir == LEFT:
        for j in range(1, range_):
            coords2.append((c_x, c_y, steps))
            steps += 1
            c_x -= 1
    elif dir == UP:
        for j in range(1, range_):
            coords2.append((c_x, c_y, steps))
            steps += 1
            c_y += 1
    elif dir == DOWN:
        for j in range(1, range_):
            coords2.append((c_x, c_y, steps))
            steps += 1
            c_y -= 1
    else:
        raise ValueError("Invalid direction found")

print(len(coords1))
print(len(coords2))

print()
print(coords1[len(coords1)-1][2])
print(coords2[len(coords2)-1][2])
print()

coords1 = sorted(coords1, key=lambda element: element[2])
coords2 = sorted(coords2, key=lambda element: element[2])


def yolo_method(coords1, coords2):
    min_ = sys.maxsize
    coords1 = coords1[1:]
    coords2 = coords2[1:]
    for coord1 in coords1:
        for coord2 in coords2:
            if coord1[0] == coord2[0] and coord1[1] == coord2[1]:
                print(f"Intersection found: x:{coord1[0]}, y:{coord1[1]}")
                tmp = coord1[2] + coord2[2]
                if tmp < min_:
                    min_ = tmp
                    print(f"Cur min:{min_}\n")
            if coord1[2] + coord2[2] >= min_:
                break
    return min_


min_value = yolo_method(coords1, coords2)
print(min_value)

"""
min_ = sys.maxsize
for match in matches:
    tmp = match
    if tmp < min_:
        min_ = tmp

print(min_)


def calc_manhattan(coords):
    x1 = 0
    y1 = 0
    x2 = coords[0]
    y2 = coords[1]
    return math.fabs(x1 - x2) + math.fabs(y1 - y2)


min_ = sys.maxsize
for match in matches:
    curr = calc_manhattan(match)
    if curr < min_:
        min_ = curr

#print(min_)
"""

# solution: 1431
# solution_2: 48014 too high