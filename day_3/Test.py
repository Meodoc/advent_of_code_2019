coords1 = [(1, 1, 1), (5, 7, 1), (10, 11, 1)]
coords2 = [(1, 1, 2), (5, 7, 2), (10, 11, 2)]

coords = coords1 + coords2
coords = sorted(coords, key=lambda element: (element[0], element[1]))

for coord in coords:
    print(coord)