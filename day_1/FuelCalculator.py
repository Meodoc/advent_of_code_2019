import math

contents = None
with open('data.in') as f:
    contents = f.read()

total_fuel = 0
# go over all lines
for line in contents.splitlines():
    mass = float(line)
    fuel_for_mass = math.floor(mass / 3) - 2
    while fuel_for_mass > 0:
        total_fuel += fuel_for_mass
        fuel_for_mass = math.floor(fuel_for_mass / 3) - 2

print(total_fuel)
